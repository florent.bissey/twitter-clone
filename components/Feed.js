import Post from "./Post";
import { SparklesIcon } from "@heroicons/react/outline";
import Input from "./Input";

import { useState } from "react";

export default function Feed() {
const listposts = [
    {
        id:  "1",
        name: "Florent Bissey",
        username: "flobis",
        userImg: "https://pbs.twimg.com/profile_images/1476699576706998272/c8_tmWEC_400x400.jpg",
        img: "https://prmeng.rosselcdn.net/sites/default/files/dpistyles_v2/prm_16_9_856w/2021/06/05/node_199876/38533733/public/2021/06/05/B9727268592Z.1_20210605174252_000%2BGESI91VNU.2-0.jpg?itok=WCLteKcQ1622908558",
        text: "nice view",
        timestamp: "two hours ago"
    },
    {
        id:  "2",
        name: "Florent Bissey",
        username: "flobis",
        userImg: "https://pbs.twimg.com/profile_images/1476699576706998272/c8_tmWEC_400x400.jpg",
        img: "https://images.unsplash.com/photo-1658171403119-d564e227907f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
        text: "love",
        timestamp: "2 days ago"
    },
]

const [posts, setposts] = useState(listposts);

return (
    <div className="xl:ml-[370px] border-l border-r border-gray-200 xl:min-w-[576px] sm:ml-[73px] flex-grow max-w-xl">
        <div className="flex py-2 px-3 sticky top-0 z-50 bg-white border-b border-gray-200">
            <h2 className="text-lg sm:text-xl font-bold cursor-pointer">Home</h2>
            <div className="hoverEffect flex items-center justify-center px-0 ml-auto w-9 h-9">
                <SparklesIcon className="h-5 "/>
            </div>
        </div>
        <Input />
        <div>
            {/* {posts.map( post => 
                <div key = {post.id}>
                    <h5>{post.username}</h5>
                </div>)
            } */}
            {posts.map( post => 
                <Post key={post.id} post={post} />
            )}
        </div>
    </div>
  )
}
