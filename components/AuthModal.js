import { useState } from "react";
import Connexion from "./Connexion";
import Inscription from "./Inscription";


  const AuthModal  = () => {
    const [showInscirptionModal, setShowInscriptionModal] = useState(false);
    const [showConnexionModal, setShowConnexionModal] = useState(false);

    return (
      <div className="flex md:flex-col justify-center items-center mt-40">
        <div className="flex gap-5">
          <button 
            type="button"
            className="bg-blue-400 text-white rounded-full w-56 h-12 font-bold shadow-md hover:brightness-95 text-lg hidden xl:inline"
            onClick={() => {setShowInscriptionModal(true);setShowConnexionModal(false)}}
          >
            S'inscrire
          </button>
          <button
            type="button" 
            className="bg-blue-400 text-white rounded-full w-56 h-12 font-bold shadow-md hover:brightness-95 text-lg hidden xl:inline"
            onClick={() => {setShowConnexionModal(true);setShowInscriptionModal(false)}}
          >
            Se connecter
          </button>
        </div>
        {/* First Modal */}
        {showInscirptionModal ? (
          <Inscription />
        ) : null}

        {/* Second Modal */}
        {showConnexionModal ? (
          <Connexion />
        ) : null}
      </div>
    )
  }

  export default AuthModal;
