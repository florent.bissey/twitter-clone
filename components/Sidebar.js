import Image from "next/image";
import SidebarMenuItem from "./SidebarMenuItem";
import {HomeIcon} from "@heroicons/react/solid"
import {BellIcon, BookmarkIcon, ClipboardIcon, DotsCircleHorizontalIcon, DotsHorizontalIcon, HashtagIcon, InboxIcon, UserIcon} from "@heroicons/react/outline"

export default function Sidebar() {
  return (
    <div className="hidden sm:flex flex-col p-2 xl:items-start fixed h-full xl:ml-24">

        {/* Logo */}
        <div className="hoverEffect p-0 hover:bg-blue-100 xl:px-1">
            <Image width="50" height="50" src="https://upload.wikimedia.org/wikipedia/fr/thumb/c/c8/Twitter_Bird.svg/2518px-Twitter_Bird.svg.png"></Image>
        </div>

        {/* Menu */}
        <div className="mt-4 mb-2.5 xl:items-start">
            <SidebarMenuItem text="Accueil " Icon={HomeIcon} active />
            <SidebarMenuItem text="Explorer" Icon={HashtagIcon}/>
            <SidebarMenuItem text="Notifications" Icon={BellIcon}/>
            <SidebarMenuItem text="Messages" Icon={InboxIcon}/>
            <SidebarMenuItem text="Signets" Icon={BookmarkIcon}/>
            <SidebarMenuItem text="Listes" Icon={ClipboardIcon}/>
            <SidebarMenuItem text="Profil" Icon={UserIcon}/>
            <SidebarMenuItem text="Plus" Icon={DotsCircleHorizontalIcon}/>
        </div>

        {/* Button */}

        <button className="bg-blue-400 text-white rounded-full w-56 h-12 font-bold shadow-md hover:brightness-95 text-lg hidden xl:inline">Tweeter</button>

        {/* Profile */}

        <div className="hoverEffect text-gray-700 flex items-center justify-center xl:justify-start mt-auto">
            <img 
                src="https://pbs.twimg.com/profile_images/1476699576706998272/c8_tmWEC_400x400.jpg" 
                alt="user-img"
                className="h-10 w-10 rounded-full xl:mr-2"
            />
            <div className="leading-5 hidden xl:inline">
                <h4 className="font-bold">Florent Bissey</h4>
                <p className="text-gray-500">@flobis</p>
            </div>
            <DotsHorizontalIcon className="h-5 xl-ml-8"/>
        </div>

    </div>
  )
}
